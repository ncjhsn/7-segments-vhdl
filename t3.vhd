library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity circuito_logico_aa3 is 
    Port(
        letra       : in std_logic_vector (3 downto 0);
        sel_tipo    : in std_logic;
        display     : out std_logic_vector(6 downto 0)
    );
end circuito_logico_aa3;

architecture nicolas_pietro_silva of circuito_logico_aa3 is
    signal common_anode     : std_logic_vector (6 downto 0);
    signal common_cathode   : std_logic_vector (6 downto 0);
begin
    display <= common_anode when sel_tipo = '0' else common_cathode;

    common_anode_process: process(letra)
        begin
            case letra is
                when '0000'  => common_anode <="0001000";
                when '0001'  => common_anode <="0000011"; 
                when '0010'  => common_anode <="1000110"; 
                when '0011'  => common_anode <="0100001";
                when '0100'  => common_anode <="0000110";
                when '0101'  => common_anode <="0001110";
                when '0110'  => common_anode <="1000000";
                when '0111'  => common_anode <="1111001";
                when '1000'  => common_anode <="0100100";
                when '1001'  => common_anode <="0110000";
                when '1010'  => common_anode <="0011001";         
                when '1011'  => common_anode <="0010010";
                when '1100'  => common_anode <="0000010";
                when '1101'  => common_anode <="1111000";
                when '1110'  => common_anode <="0000000";
                when '1111'  => common_anode <="0010000";
            end case;
    end process;

    common_cathode_process: process(letra)
    begin
        case letra is
            when '0000'  => common_cathode <="1110111";
            when '0001'  => common_cathode <="1111100";
            when '0010'  => common_cathode <="0111001";
            when '0011'  => common_cathode <="1011110";
            when '0100'  => common_cathode <="1111001";
            when '0101'  => common_cathode <="1110001";
            when '0110'  => common_cathode <="0111111";
            when '0111'  => common_cathode <="0000110";
            when '1000'  => common_cathode <="1011011";
            when '1001'  => common_cathode <="1001111";
            when '1010'  => common_cathode <="1100110"; 
            when '1011'  => common_cathode <="1101101";
            when '1100'  => common_cathode <="1111101";
            when '1101'  => common_cathode <="0000111";
            when '1110'  => common_cathode <="1111111";
            when '1111'  => common_cathode <="1101111";
        end case;
end process;
end nicolas_pietro_silva;

